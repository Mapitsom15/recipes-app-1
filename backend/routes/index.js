/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);
const appRoot = process.cwd();

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	api: importRoutes('./api'),
};

// Setup Route Bindings
exports = module.exports = function (app) {

	app.use(function (req, res, next) {
		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
		res.header('Access-Control-Allow-Headers', 'Authorization, Content-Type, X-Frame-Options');
		res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
		next();
	 });
	 
	 app.options('*', (req, res) => res.sendStatus(200) )

	// api
	app.get('/api/recipes/list', keystone.middleware.api, routes.api.recipes.list); //all recipes
	app.get('/api/recipes/delete/:id', keystone.middleware.api, routes.api.recipes.remove); //delete recipe
	app.get('/api/recipes/:recipeId', keystone.middleware.api, routes.api.recipes.get); //single recipe
	app.get('/api/recipes/search/:searchString', keystone.middleware.api, routes.api.recipes.search); //single recipe
	app.post('/api/recipes/create', keystone.middleware.api, routes.api.recipes.create);
	
	app.all('/api/recipes/:id/update', keystone.middleware.api, routes.api.recipes.update);
	
	 app.get('*', (req, res) => {
		res.sendFile(appRoot + '\\frontend\\index.html')
	 })

};
