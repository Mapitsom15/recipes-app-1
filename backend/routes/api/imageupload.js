const async = require("async");
const keystone = require("keystone");

const ImageUpload = keystone.list("ImageUpload");
const moveFileModule = require("./moveFile");
const appRoot = process.cwd();

//Add Image
exports.create = function(req, res) {
	const data = req.method == "POST" ? req.body : req.query;
	const fileData = req.files.file;

	if (fileData && fileData.path) {
		const current_path = fileData.path;
		const new_path = appRoot + "/public/uploads/files/" + fileData.originalname;
		
		moveFileModule(current_path, new_path, (error) => {
			if (error) return res.json({ error: error });
			
			const file = {
				filename: fileData.originalname,
				size: fileData.size,
				mimetype: fileData.mimetype,
				path: "uploads/files/" + fileData.originalname,
				originalname: fileData.originalname,
				url: 'localhost:3000/uploads/files/' + fileData.originalname 
			};

			new ImageUpload.model({file})
			.save((err, data) => {
				console.log(err)
				console.log(data)
				res.apiResponse({ data });
			})
		});

	} else {
		return res.json({ error: "empty file not allowed" });
	}
};
