const async = require("async");
const keystone = require("keystone");
const Recipes = keystone.list("Recipes");
const moveFileModule = require("./moveFile");
const appRoot = process.cwd();
var moment = require('moment');

// List Recipes
exports.list = function(req, res) {
	Recipes.model
		.find()
		.where("state", "published")
		.exec(function(err, items) {
			if (err) return res.apiError("database error", err);

			res.apiResponse(items);
		});
};

//Get single recipe
exports.get = function(req, res) {
	Recipes.model
		.findById(req.params.recipeId)
		.populate("title")
		.exc(function(err, post) {
			if (err) return res.err(err);
			if (!recipe) return res.notfound("Recipe not found");
			console.log(recipes);
		});
};

exports.search = function(req, res) {
	console.log("=== Search : searchString = " + req.params.searchString);
	Recipes.model
		.find({ title: { $regex: new RegExp(req.params.searchString, "i") } })
		.exec(function(err, recipes) {
			if (err) {
				console.log("--- ERROR ---");
				console.log(err);
				res.sendStatus(500);
			} else {
				if (!recipes) {
					console.err("--- Recipe not found ---");
					return res.notfound;
				} else {
					console.log("--- Found " + recipes.length + " recipe ---");
					console.log(recipes);
					res.send(recipes);
				}
			}
		});
};

//Add recipe
exports.create = function(req, res) {
	const data = req.method == "POST" ? req.body : req.query;
	const fileData = req.files.image;

	const title = data.title;
	const author = data.author;
	const ingredients = data.ingredients;
	const prepTime = data.prepTime;
	const cookTime = data.cookTime;
	const steps = data.steps;
	const state = data.state;

	if (fileData && fileData.path) {
		const current_path = fileData.path;
		const new_path = appRoot + "/public/uploads/files/" + fileData.originalname;

		moveFileModule(current_path, new_path, error => {
			if (error) return res.json({ error: error });

			const image = {
				filename: fileData.originalname,
				size: fileData.size,
				mimetype: fileData.mimetype,
				path: "uploads/files/" + fileData.originalname,
				originalname: fileData.originalname,
				url: "http://localhost:3000/uploads/files/" + fileData.originalname
			};

			new Recipes.model({
				title,
				author,
				ingredients,
				prepTime,
				cookTime,
				steps,
				state,
				image
			}).save((err, data) => {
				console.log(err);
				console.log(data);
				res.apiResponse({ response: "successful", data });
			});
		});
	} else {
		return res.json({ response: "empty_file" });
	}
};

//Update recipe
exports.update = function(req, res) {
	Recipes.model.findById(req.params.id).exec(function(err, item) {
		if (err) return res.apiError("database error", err);
		if (!item) return res.apiError("not found");

		var data = req.method == "POST" ? req.body : req.query;

		item.getUpdateHandler(req).process(data, function(err) {
			if (err) return res.apiError("create error", err);

			res.apiResponse({
				collection: item
			});
		});
	});
	
};

//Delete recipe
exports.remove = function(req, res) {
	console.log("=== Attempting to delete recipe ===");

	var now = moment();
	
	Recipes.model.findOne({ _id: req.params.id}, function (err, recipe) {
		// First check if recipe exists
		if (err) {
			console.log(err);
			res.status(500).send();
		}

		if (recipe) {
			console.log(recipe);
			daysExisting = now.diff(moment(recipe.createdAt), "days");
			// If recipe exists, and older then 10 days, attempt to delete
			if (daysExisting > 10) {
				recipe.remove({}, function(err) {
					// If delete operation is successful
					res.apiResponse({ status: "success", message: "recipe deleted" });
					console.log("recipe deleted");
				});
			} else {
				res.apiResponse({ status: "failed",  message: "recipe not old enough" });
				console.log('recipe not old enough');
			}
		} else {
			res.apiResponse({ status: "failed",  message: "recipe not found"  });
			console.log('recipe not found');
		}
	});

}
