var keystone = require('keystone');
var Types = keystone.Field.Types;

/*Image Upload model*/
var ImageUpload = new keystone.List('ImageUpload', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true },
	track: true
})


var myStorage = new keystone.Storage({
	adapter: keystone.Storage.Adapters.FS,
	fs: {
	  path: keystone.expandPath('./public/uploads/files'), // required; path where the files should be stored
	  publicPath: '/public/uploads/files', // path where files will be served
	}
  });

  ImageUpload.add({
	name: { type: Types.Key, index: true},
	file: {
	  type: Types.File,
	  storage: myStorage
	},
	createdTimeStamp: { type: String },
	alt1: { type: String },
	attributes1: { type: String },
	category: { type: String },      
	parent: { type: String },
	children: { type: String },
	url: {type: String},
	fileType: {type: String}
  
  });
  
  ImageUpload.defaultColumns = 'name';
  ImageUpload.register();
