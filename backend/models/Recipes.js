var keystone = require("keystone");
var Types = keystone.Field.Types;

/**
 * Recipe Model
 * ==========
 */

var Recipes = new keystone.List("Recipes", {
	map: { name: "title" },
	autokey: { path: "slug", from: "title", unique: true },
	track: true
});

var myStorage = new keystone.Storage({
	adapter: keystone.Storage.Adapters.FS,
	fs: {
		path: keystone.expandPath("./public/uploads/files"), // required; path where the files should be stored
		publicPath: "/public/uploads/files" // path where files will be served
	},
	schema: {
		originalname: true,
		url: true
	}
});

Recipes.add({
	title: { type: String, required: true },
	state: {
		type: Types.Select,
		options: "draft, published, archived",
		default: "draft",
		index: true
	},
	author: { type: String },
	ingredients: {
		type: Types.Html,
		wysiwyg: true,
		height: 150
	},
	prepTime: { type: String },
	cookTime: { type: String },
	steps: { type: Types.Textarea },
	image: {
		type: Types.File,
		storage: myStorage
	}
});

Recipes.defaultColumns = "title, state|20%, author|20%, publishedDate|20%";
Recipes.register();
