import { Component, OnInit, Input } from '@angular/core';
import { RecipesService } from '../recipes.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-recipes-add',
  templateUrl: './recipes-add.component.html',
  styleUrls: ['./recipes-add.component.scss']
})
export class RecipesAddComponent implements OnInit {

  @Input() recipesData = { title: '', author: '',
  ingredients: '', prepTime: '',
  cookTime: '', steps: '', };

  constructor(public recipes: RecipesService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

  }


  addRecipes() {
    this.recipes.addRecipes(this.recipesData).subscribe((result) => {
      this.router.navigate(['/recipes-add/' + result._id]);
    }, (err) => {
      console.log(err);
    });
  }

}
