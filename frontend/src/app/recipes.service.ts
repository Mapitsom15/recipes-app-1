import { environment } from '../environments/environment';
import { share } from 'rxjs/internal/operators';
import { Title } from '@angular/platform-browser/src/browser/title';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse  } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { RecipesComponent } from 'src/app/recipes/recipes.component';

const endpoint = 'http://localhost:3000/api/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

const httpOptions2 = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RecipesService {

  constructor(private http: HttpClient) {

  }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }

  getRecipes(): Observable<any> {
    return this.http.get(endpoint + 'recipes/list/').pipe(
      map(this.extractData));
  }

  getRecipe(id): Observable<any> {
    return this.http.get(endpoint + 'recipes/recipes' + id).pipe(
      map(this.extractData));
  }


  searchRecipe(data): Observable<any> {
    console.log('--> ' + data);

    return this.http.get(endpoint + 'recipes/search/' + data ).pipe(
      map(this.extractData));

  }

  addRecipes (recipe): Observable<any> {
    console.log(recipe);
    return this.http.post<any>(endpoint + 'recipes/create', recipe).pipe(
      tap((item) => console.log(`added recipe w/ id=${item.id}`)),
      catchError(this.handleError<any>('addRecipe'))
    );
  }

  removeRecipe(_id): Observable<any> {
    return this.http.get(endpoint + `recipes/delete/${_id}`).pipe(
      tap(_  => console.log(`deleted recipes id=${_id}`)),
      catchError(this.handleError<any>('removeRecipe'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
