import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { RecipeTopComponent } from './recipe-top/recipe-top.component';
import { RecipeContentComponent } from './recipe-content/recipe-content.component';
import { GalleryComponent } from './gallery/gallery.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import {
  RecipesComponent, RecipesDialogComponent, RecipesDeleteDialogComponent,
  RecipesResultDialogComponent } from './recipes/recipes.component';
import { ContactComponent } from './contact/contact.component';
import { FormComponent } from './form/form.component';
import { NameEditorComponent } from './name-editor/name-editor.component';
import { ProfileEditorComponent } from './profile-editor/profile-editor.component';
import { RecipesAddComponent } from './recipes-add/recipes-add.component';
import { RecipesDetailComponent } from './recipes-detail/recipes-detail.component';


const appRoutes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'recipes', component: RecipesComponent,  data: { title: 'Recipe List' } },
    { path: 'contact', component: ContactComponent },
    { path: 'recipe-detail/:id', component: RecipesDetailComponent, data: { title: 'Recipe Details' } },
    { path: 'recipes-add', component: RecipesAddComponent},
    { path: '**', redirectTo: 'home', pathMatch: 'full' },
];

export const APP_ROUTES = {
    components: [
        MenuComponent,
        RecipeTopComponent,
        RecipeContentComponent,
        GalleryComponent,
        FooterComponent,
        ContactComponent,
        HomeComponent,
        FormComponent,
        NameEditorComponent,
        ProfileEditorComponent,
        RecipesComponent,
        RecipesDialogComponent,
        RecipesAddComponent,
        RecipesDetailComponent,
        RecipesDeleteDialogComponent,
        RecipesResultDialogComponent

    ],
    routes: appRoutes,
    entryComponent: [ RecipesDialogComponent,
      RecipesDeleteDialogComponent, RecipesResultDialogComponent]
};
