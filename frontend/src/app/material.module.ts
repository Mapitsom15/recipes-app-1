import { NgModule } from '@angular/core';

import {
MatButtonModule,
MatToolbarModule,
MatFormFieldModule,
MatInputModule,
MatDialogModule,
MatCardModule,
MatDividerModule
} from '@angular/material';

@NgModule({
imports: [
MatButtonModule,
MatToolbarModule,
MatInputModule,
MatFormFieldModule,
MatCardModule,
MatDividerModule

],
exports: [
MatButtonModule,
MatToolbarModule,
MatInputModule,
MatFormFieldModule,
MatDialogModule,
MatCardModule,
MatDividerModule
]
})

export class MaterialModule { }
