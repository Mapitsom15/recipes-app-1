import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeTopComponent } from './recipe-top.component';

describe('RecipeTopComponent', () => {
  let component: RecipeTopComponent;
  let fixture: ComponentFixture<RecipeTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
