import { RecipesService } from '../../recipes.service';
import { Component, OnInit, ElementRef, ViewChild, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-recipes-result-dialog',
  templateUrl: './recipes-result-dialog.component.html',
  styleUrls: ['./recipes-result-dialog.component.css']
})
export class RecipesResultDialogComponent implements OnInit {
  recipesData: any = {};

  constructor(
    public dialogRef: MatDialogRef<RecipesResultDialogComponent>,
    public dialog: MatDialog,
    public recipes: RecipesService,
    private router: Router,
    private httpClient: HttpClient,
    @Inject(MAT_DIALOG_DATA) public result: any) {
  }

  ngOnInit() {}

  dialogClose() {
    this.dialogRef.close();
  }
}
