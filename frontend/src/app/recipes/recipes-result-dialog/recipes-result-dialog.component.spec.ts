import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipesResultDialogComponent } from './recipes-result-dialog.component';

describe('RecipesResultDialogComponent', () => {
  let component: RecipesResultDialogComponent;
  let fixture: ComponentFixture<RecipesResultDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipesResultDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipesResultDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
