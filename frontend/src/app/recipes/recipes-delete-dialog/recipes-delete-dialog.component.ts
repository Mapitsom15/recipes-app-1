import { RecipesService } from '../../recipes.service';
import { Component, OnInit, ElementRef, ViewChild, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-recipes-delete-dialog',
  templateUrl: './recipes-delete-dialog.component.html',
  styleUrls: ['./recipes-delete-dialog.component.css']
})
export class RecipesDeleteDialogComponent implements OnInit {
  recipesData: any = {};

  constructor(
    public dialogRef: MatDialogRef<RecipesDeleteDialogComponent>,
    public dialog: MatDialog,
    public recipes: RecipesService,
    private router: Router,
    private httpClient: HttpClient,
    @Inject(MAT_DIALOG_DATA) public recipe: any
  ) {}

  ngOnInit() {}

  dialogClose(result: boolean) {
    this.dialogRef.close(result);
  }

  /*
  removeRecipe(id): void {
    this.recipes = this.recipes;
    this.recipes.removeRecipe(id)
    .subscribe( (result) => {
        console.log(result);
        this.dialogRef.close({ result: 'successful', id });

        if (result.status === 'success') {
          console.log('SUCCESS');
        } else {
          const dialogRef = this.dialog.open(RecipesDeleteDialogComponent, {
            width: '250px',

          });
          console.log('UNABLE to DELETE : ' + result.message);
        }
    }, (err) => {
        console.error(err);
        this.dialogRef.close({ result: 'error' });
    });
  }
  */
}






// }


