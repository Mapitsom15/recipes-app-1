import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RecipesService } from '../../recipes.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-recipes-dialog',
  templateUrl: './recipes-dialog.component.html',
  styleUrls: ['./recipes-dialog.component.css']
})
export class RecipesDialogComponent implements OnInit {
  recipesData: any = {};
  imageFile: any = null;

  constructor(
    public dialogRef: MatDialogRef<RecipesDialogComponent>,
    public dialog: MatDialog,
    public recipes: RecipesService,
    private router: Router,
    private httpClient: HttpClient) {
    this.recipesData = {
      title: '',
      author: '',
      ingredients: '',
      prepTime: '',
      cookTime: '',
      steps: '',
      state: 'published',
    };
  }

  ngOnInit() {}

  addRecipes() {
    const input = new FormData();
    input.append('title', this.recipesData.title);
    input.append('author', this.recipesData.author);
    input.append('ingredients', this.recipesData.ingredients);
    input.append('prepTime', this.recipesData.prepTime);
    input.append('cookTime', this.recipesData.cookTime);
    input.append('steps', this.recipesData.steps);
    input.append('state', this.recipesData.state);
    input.append('image', this.imageFile, this.imageFile.name);
    this.recipes.addRecipes(input).subscribe((result) => {
      if (result.response === 'successful') {
        this.dialogRef.close();
      } else if (result.response === 'empty_file') {
        alert('empty file not allowed');
      } else {
        console.error(result);
        alert('failed to upload');
      }
    }, (err) => {
      console.error(err);
      alert('failed to upload');
    });
  }

  selectFileUpload(event) {
    this.imageFile =  event.target.files.item(0);
  }

}
