import { Component, OnInit } from '@angular/core';
import { RecipesService } from '../recipes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material';
import { RecipesDialogComponent } from './recipes-dialog/recipes-dialog.component';
export { RecipesDialogComponent } from './recipes-dialog/recipes-dialog.component';
import { RecipesDeleteDialogComponent } from './recipes-delete-dialog/recipes-delete-dialog.component';
export { RecipesDeleteDialogComponent } from './recipes-delete-dialog/recipes-delete-dialog.component';
import { RecipesResultDialogComponent } from './recipes-result-dialog/recipes-result-dialog.component';
export { RecipesResultDialogComponent } from './recipes-result-dialog/recipes-result-dialog.component';
import {MatCardModule} from '@angular/material/card';
import { JsonpCallbackContext } from '@angular/common/http/src/jsonp';
import { Title } from '@angular/platform-browser/src/browser/title';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss']
})
export class RecipesComponent implements OnInit {
  recipes: any[] = [];
  searchString: any;
  deleteDialogRef: MatDialogRef<RecipesDeleteDialogComponent>;
  addDialogRef: MatDialogRef<RecipesDialogComponent>;
  resultDialogRef: MatDialogRef<RecipesResultDialogComponent>;
  constructor(
    public recipesService: RecipesService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.getRecipes();


  }

  // get recipes
  getRecipes() {
    this.recipes = [];
    this.recipesService.getRecipes().subscribe((data: any[]) => {
    this.recipes = data;
    });
  }

  // open add recipe dialog
  addRecipes() {
    this.addDialogRef = this.dialog.open(RecipesDialogComponent, {
      width: '250px',
    });

    this.addDialogRef.afterClosed().subscribe(result => {
      this.getRecipes();
    });
  }

 // search for recipe
  searchRecipe() {
    console.log('Attempting to search for string = ' + this.searchString);
    this.recipesService.searchRecipe(this.searchString).subscribe((searchResults: any[]) => {
      this.recipes = searchResults;
    });
  }
// open delete recipe dialog
  removeRecipe(recipe): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = recipe;

    this.deleteDialogRef = this.dialog.open(RecipesDeleteDialogComponent, dialogConfig);
// if data is true remove selcted recipe
    this.deleteDialogRef.afterClosed().subscribe(data => {
        if (data === true) {
          this.deleteTheRecipe(recipe._id);
        }
    });
  }

  // delete recipe call
  deleteTheRecipe(id): void {
    const dialogConfig = new MatDialogConfig();

    this.recipesService.removeRecipe(id)
    .subscribe( (result) => {
        this.deleteDialogRef.close();
        dialogConfig.data = result;
        this.resultDialogRef = this.dialog.open(RecipesResultDialogComponent, dialogConfig);
        this.getRecipes();
    }, (err) => {
        console.error(err);
        this.deleteDialogRef.close({ result: 'error' });
    });
  }

}
